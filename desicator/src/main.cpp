//
// Created by Richard Weiss on 2020-06-29.
//

#include <Arduino.h>
#include <Wire.h>
#include <SPI.h>

#include <Adafruit_Sensor.h>
#include "Adafruit_BME680.h"

#define ARDUINOJSON_USE_LONG_LONG 1
#include <ArduinoJson.h>
#include <NTPClient.h>
#include <WiFiUdp.h>

#include "networking.hpp"

WiFiUDP ntpUDP;
NTPClient timeClient(ntpUDP, "europe.pool.ntp.org", 3600, 60000);
Adafruit_BME680 bme;

//OLED oled(4, 2, 15);
OLED oled(24, 22, 21);


void setup(void) {
    Serial.begin(9600);

    while (!bme.begin()) {
        Serial.println("Could not find sensor!");
        delay(1000);
    }

    bme.setTemperatureOversampling(BME680_OS_8X);
    bme.setHumidityOversampling(BME680_OS_2X);
    bme.setPressureOversampling(BME680_OS_4X);
    bme.setIIRFilterSize(BME680_FILTER_SIZE_3);

    setup_net();
    timeClient.begin();
    timeClient.update();

    oled.begin();
}

void loop(void) {
    static StaticJsonDocument<200> doc;
    oled.drawLine(32, 32, 96, 96, RED);

    if (!bme.performReading()) {
        Serial.println("Failed to perform reading :(");
    } else {
        if (random(0, 1000) <= 1) {
            Serial.println("Updating timestamp");
            timeClient.update();
        }

        doc["temperature"] = bme.temperature;
        doc["pressure"] = bme.pressure;
        doc["humidity"] = bme.humidity;

        long long epoch = timeClient.getEpochTime();
        doc["timestamp_ms"] = epoch * 1000;

        String output;
        serializeJson(doc, output);

        Serial.println(output);
        send_message("desicator-top-sensor", output.c_str());
    }
    delay(1000);
}