import pickle
import sys
import time

import numpy as np

import zmq


def frame_source():
    import picamera
    with picamera.PiCamera() as camera:
        camera.resolution = (320, 240)
        camera.framerate = 24
        time.sleep(2)
        output = np.empty((240, 320, 3), dtype=np.uint8)

        for _ in camera.capture_continuous(output, 'rgb', use_video_port=True):
            yield output


def dummy_frame_source():
    output = np.empty((240, 320, 3), dtype=np.uint8)
    while True:
        output[:] = 0
        output[
            np.random.randint(240),
            np.random.randint(320),
            np.random.randint(3),
        ] = 255
        yield output
        time.sleep(1/24)


def test_recv(addr):
    context = zmq.Context()
    socket = context.socket(zmq.SUB)
    socket.connect(addr)
    socket.setsockopt(zmq.SUBSCRIBE, b"")

    for _ in range(10):
        string = socket.recv()
        messagedata = pickle.loads(string)
        with open('./test.pkl', 'wb') as f:
            f.write(string)


if __name__ == '__main__':
    if len(sys.argv) > 1:
        test_recv(sys.argv[1])
    else:
        context = zmq.Context()
        socket = context.socket(zmq.PUB)
        socket.bind("tcp://*:%s" % 5555)

        for frame in dummy_frame_source():
            messagedata = pickle.dumps(frame)
            # socket.send("0 %s" % messagedata)
            socket.send(messagedata)
