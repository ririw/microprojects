use tract_onnx::prelude::*;
use std::io::Cursor;


//noinspection RsTypeCheck
static MODEL_DATA: &[u8] = std::include_bytes!("../test.onnx");

pub type ModelType = SimplePlan<TypedFact, std::boxed::Box<dyn TypedOp>, Graph<TypedFact, std::boxed::Box<dyn TypedOp>>>;

fn inner_load() -> TractResult<ModelType> {
    let mut data = Cursor::new(MODEL_DATA.to_vec());
    let input_shape = InferenceFact::dt_shape(f32::datum_type(), tvec!(64));
    let _model: ModelType = tract_onnx::onnx()
        // load the model
        .model_for_read(&mut data)?
        // specify input and output type and shape
        .with_input_fact(0, input_shape)?
        // optimize the model
        .into_optimized()?
        // make the model runnable and fix its inputs and outputs
        .into_runnable()?;
    Ok(_model)
}

fn inner_run_model(model: &ModelType, frame: &[[f64; 8]; 8], lastframe: &[[f64; 8]; 8]) -> TractResult<u8> {
    let mut framevec = Vec::new();
    let mut lastframevec = Vec::new();
    for r in 0..8 {
        for c in 0..8 {
            framevec.push(frame[r][c] as f32);
            lastframevec.push(lastframe[r][c] as f32);
        }
    }
    let tensor = tract_ndarray::Array1::from(framevec).into();
//    let lasttensor = tract_ndarray::Array1::from(lastframevec).into();
    let res = model.run(tvec!(tensor))?;
    let finalres = *(res[0].to_array_view::<i64>()?.iter().next().unwrap());

    Ok(finalres as u8)
}


pub fn load() -> anyhow::Result<ModelType> {
    match inner_load() {
        Ok(v) => Ok(v),
        Err(TractError(kind, st)) => Err(anyhow::anyhow!("Error loading model: {:?} -- {:?}", kind, st))
    }
}


pub fn run_model(model: &ModelType, frame: &[[f64; 8]; 8], lastframe: &[[f64; 8]; 8]) -> anyhow::Result<u8> {
    match inner_run_model(model, frame, lastframe) {
        Ok(v) => Ok(v),
        Err(TractError(kind, st)) => Err(anyhow::anyhow!("Error running model: {:?} -- {:?}", kind, st))
    }
}
