use std::fmt::{Formatter, Error};

/// The state machine exposes a counter of the number of people
/// that have incremented or decremented it. It also keeps track
/// of some stuff needed to work out which state it'll be in next.
#[derive(Debug, Copy, Clone)]
pub struct StateMachine {
    pub counter: i32,
    subcounter: i32,
    last_state: i32,
}

impl std::fmt::Display for StateMachine {
    fn fmt(&self, f: &mut Formatter) -> Result<(), Error> {
        write!(f, "StateMachine({})", self.counter)
    }
}

/// Action represet the actions taken by the state machine. This is mostly
/// useful for translating state machine transitions into actions that affect
/// the outside world.
pub enum Action {
    Noop,
    Inc,
    Dec
}

impl StateMachine {
    pub fn new() -> Self {
        StateMachine {
            counter: 0,
            subcounter: 0,
            last_state: 0
        }
    }
    pub fn act(&self, readout: i32) -> (Self, Action) {
        let subcount = if readout != self.last_state {
            0
        } else {
            self.subcounter + 1
        };

        let mut new_state = StateMachine {
            counter: self.counter,
            subcounter: subcount,
            last_state: readout,
        };
        let last = self.last_state;
        let action = match (last, readout) {
            (0, 0) => Action::Noop,
            (0, 1) if self.subcounter > 5 => {new_state.counter += 1; Action::Inc},
            (0, 1) => Action::Noop,
            (0, 2) if self.subcounter > 5 => {new_state.counter -= 1; Action::Dec},
            (0, 2) => Action::Noop,

            (2, 0) => Action::Noop,
            (2, 1) => Action::Noop,
            (2, 2) => Action::Noop,

            (1, 0) => Action::Noop,
            (1, 1) => Action::Noop,
            (1, 2) => Action::Noop,

            (_, _) => unreachable!(),
        };

        (new_state, action)
    }
}

pub struct LifxInfo {
    group_id: String,
    header: String
}

impl LifxInfo {
    /// Read out the LifxInfo from a text file. It should have
    /// exactly two lines, the first is the group id for the
    /// lights we want to turn on and off, and the second is
    /// a LIFX auth token.
    ///
    /// For example:
    ///
    /// Bearer c6...
    /// 4d0a...
    /// EOF
    pub fn new() -> LifxInfo {
        let info = include_str!("../lifx_info.txt");
        let lines: Vec<&str> = info.split("\n").collect();
        return LifxInfo {
            group_id: lines[1].to_owned(),
            header: lines[0].to_owned()
        }
    }

    /// Turn on lights
    pub fn lights_on(&self) {
        let auth = self.header.clone();
        let url = format!("https://api.lifx.com/v1/lights/group_id:{}/state", self.group_id);
        let req = ureq::put(&url).set(
            "Authorization",&auth).send_form(&[("power", "on")]);
        println!("{:?}", req);
    }

    /// Turn off lights
    pub fn lights_off(&self) {
        let auth = self.header.clone();
        let url = format!("https://api.lifx.com/v1/lights/group_id:{}/state", self.group_id);
        let req = ureq::put(&url).set(
            "Authorization",&auth).send_form(&[("power", "off")]);
        println!("{:?}", req);
    }
}

