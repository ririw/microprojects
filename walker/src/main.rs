use tokio::net::UdpSocket;
use walker::datastore::{DataStoreBackend, DataStoreMessage};
use std::net::Ipv4Addr;
use walker::amg8833::AMG8833;
use anyhow::{Result, Context, anyhow};
use tokio::time;
use log::{info, error, debug};
use pnet::ipnetwork::{IpNetwork};

/*
Things in here:

 1. Create database + schema
 2. Start up the AMG -- in own thread
 3. Monitor for button press messages
 4. Execute ad nauseam
 ???
 5. Run the cool model!
 */

#[tokio::main]
async fn main() -> Result<()> {
    pretty_env_logger::init();
    let args: Vec<String> = std::env::args().collect();
    let bind_ip = if args[1] == "auto" {
        find_address()?
    } else {
        args[1].parse().context("Parsing Bind IP")?
    };
    info!("Binding to {:?}", bind_ip);
    DataStoreBackend::start("./test.sqlite3")?;
    tokio::spawn(run_amg_scanner());
    run_udp_server(bind_ip).await
}

/// Scrounge around for a plausible address to bind to...
fn find_address() -> Result<Ipv4Addr> {
    let all_interfaces = pnet::datalink::interfaces();
    let default_interface = all_interfaces
        .iter()
        .find(|e| e.is_up() && !e.is_loopback() && !e.ips.is_empty());
    let ips = match default_interface {
        Some(i) => Ok(
            i.ips.iter()
            .find(|a| match a {IpNetwork::V4(_) => true, _ => false})
                .map(|v| if let IpNetwork::V4(a) = v {a.ip()} else {unreachable!()})
            .ok_or(anyhow!("Unable to find IPV4 interface"))),
        None => Err(anyhow!("Unable to find default interface IP"))
    }?;
    ips
}

async fn run_amg_scanner() -> Result<()> {
    let store = DataStoreBackend::start("./test.sqlite3")?;
    let mut amg = AMG8833::new()?;
    let mut interval = time::interval(time::Duration::from_millis(50));
    loop {
        interval.tick().await;
        match amg.read_pixels() {
            Ok(px) => {
                let msg = DataStoreMessage::Frame(px, chrono::Utc::now());
                match store.record(msg) {
                    Ok(()) => debug!("Logged frame {:?}", msg),
                    Err(e) => error!("Error during logging: {:?}", e),
                }
            },
            Err(e) => error!("Failed to get pixels: {:?}", e),
        }

    }
}

async fn run_udp_server(bind_ip: std::net::Ipv4Addr) -> Result<()> {
    let store = DataStoreBackend::start("./test.sqlite3")?;
    let sock = UdpSocket::bind("224.1.1.1:5007").await?;
    sock.set_broadcast(true)?;
    sock.join_multicast_v4(
        "224.1.1.1".parse().unwrap(),
        bind_ip
    )?;
    let mut buf = [0; 1024];
    loop {
        let (len, _) = sock.recv_from(&mut buf).await?;
        if &buf[0..len] == b"TOUCH" {
            let event = DataStoreMessage::Button(chrono::Utc::now());
            match store.record(event) {
                Ok(()) => info!("Logged event {:?}", event),
                Err(e) => error!("Error during logging: {:?}", e),
            }
        }
    }
}