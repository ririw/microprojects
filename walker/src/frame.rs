use serde::{Deserialize, Serialize};

#[derive(Serialize, Deserialize, Debug, Clone, Copy)]
pub struct Frame {
    pub timestamp: i64,
    pub temperature: f64,
    pub frame: [[f64; 8]; 8],
    pub classif: u8,
}

pub static EMPTY_FRAME: Frame = Frame {
    timestamp: 0,
    temperature: 0.0,
    frame: [[0.0; 8]; 8],
    classif: 0,
};
