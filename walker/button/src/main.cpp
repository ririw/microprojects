#include <esp_sleep.h>
#include <Arduino.h>
#include <WiFi.h>
#include <WiFiUdp.h>
#include <esp_task_wdt.h>

#include "secrets.hpp"

#define Threshold 40 /* Greater the value, more the sensitivity */

// Display touchpad origin
void print_wakeup_touchpad()
{
    touch_pad_t touchPin;
    touchPin = esp_sleep_get_touchpad_wakeup_status();

    switch (touchPin)
    {
    case 0:
        Serial.println("Touch detected on GPIO 4");
        break;
    case 1:
        Serial.println("Touch detected on GPIO 0");
        break;
    case 2:
        Serial.println("Touch detected on GPIO 2");
        break;
    case 3:
        Serial.println("Touch detected on GPIO 15");
        break;
    case 4:
        Serial.println("Touch detected on GPIO 13");
        break;
    case 5:
        Serial.println("Touch detected on GPIO 12");
        break;
    case 6:
        Serial.println("Touch detected on GPIO 14");
        break;
    case 7:
        Serial.println("Touch detected on GPIO 27");
        break;
    case 8:
        Serial.println("Touch detected on GPIO 33");
        break;
    case 9:
        Serial.println("Touch detected on GPIO 32");
        break;
    default:
        Serial.println("Wakeup not by touchpad");
        break;
    }
}

// Display wakeup origine
void print_wakeup_reason()
{
    esp_sleep_wakeup_cause_t wakeup_reason;

    wakeup_reason = esp_sleep_get_wakeup_cause();

    switch (wakeup_reason)
    {
    case ESP_SLEEP_WAKEUP_EXT0:
        Serial.println("Wakeup caused by external signal using RTC_IO");
        break;
    case ESP_SLEEP_WAKEUP_EXT1:
        Serial.println("Wakeup caused by external signal using RTC_CNTL");
        break;
    case ESP_SLEEP_WAKEUP_TIMER:
        Serial.println("Wakeup caused by timer");
        break;
    case ESP_SLEEP_WAKEUP_TOUCHPAD:
        Serial.println("Wakeup caused by touchpad");
        break;
    case ESP_SLEEP_WAKEUP_ULP:
        Serial.println("Wakeup caused by ULP program");
        break;
    case ESP_SLEEP_WAKEUP_GPIO:
        Serial.println("Wakeup caused by GPIO");
        break;
    case ESP_SLEEP_WAKEUP_UART:
        Serial.println("Wakeup caused by UART");
        break;
    default:
        Serial.printf("Wakeup was not caused by deep sleep: %d\n", wakeup_reason);
        break;
    }
}

#define STRLEN(s) (sizeof(s)/sizeof(s[0]))

void sendBeacon()
{
    // Connect to wifi, then send a UDP packet
    // that notes that the touch has occurred.
    WiFi.begin(ssid, password);
    while (WiFi.status() != WL_CONNECTED)
    {
        delay(1000);
        Serial.print("*");
    }

    WiFiUDP Udp;
    Udp.beginPacket("224.1.1.1", 5007);
    uint8_t packet[] = "TOUCH";
    Udp.write(packet, STRLEN(packet) - 1); // -1 to drop NULL at the end.
    Udp.endPacket();
}

// Execute this function when Touch Pad in pressed
void callback()
{
    Serial.println("Touch pad woke us up...");
}

void setup()
{
    esp_task_wdt_init(30, true); //enable panic so ESP32 restarts after 30s
    esp_task_wdt_add(NULL); //add current thread to WDT watch
    Serial.begin(9600);

    // Set up our blinky LED 
    pinMode(LED_BUILTIN, OUTPUT);

    // Whenever we wake, we get the cause. If it was a touch,
    // we send a packet, otherwise just go back to sleep.
    esp_sleep_wakeup_cause_t wakeup_reason = esp_sleep_get_wakeup_cause();
    switch (wakeup_reason)
    {
    case ESP_SLEEP_WAKEUP_TOUCHPAD:
        Serial.println("Touch wakeup - sending message");
        digitalWrite(LED_BUILTIN, HIGH);
        sendBeacon();
        break;
    default:
        Serial.printf("Wakeup was not caused by deep sleep: %d\n", wakeup_reason);
    }

    // Blink... it's so comforting
    for (int i = 0; i < 5; i++)
    {
        digitalWrite(LED_BUILTIN, LOW);
        delay(50);
        digitalWrite(LED_BUILTIN, HIGH);
        delay(50);
    }

    // Enable a touch wakeup, and enable the touch wake
    touchAttachInterrupt(T6, callback, Threshold);
    esp_sleep_enable_touchpad_wakeup();

    // Disable WDT
    esp_task_wdt_delete(NULL);
    esp_task_wdt_deinit();
    // Go into deeeeep sleep
    Serial.println("Sleeping...");
    esp_deep_sleep_start();
}


// All work happens in setup - so there's no code here.
void loop()
{
}
